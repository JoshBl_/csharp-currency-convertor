using currencySwap;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using Xunit;

namespace Unit_Tests
{
    public class UnitTest1
    {
        [Fact]
        //ensures all rates are pulled
        public void RatesArePulled()
        {
            //arrange
            string html = string.Empty;
            string url = "https://api.exchangeratesapi.io/latest?base=GBP";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;
            //new local variable to store the HTTP response
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            //new local variable to read the body of the response from the server
            using (Stream stream = response.GetResponseStream())
            //new local variable to read characters from a byte stream
            using (StreamReader reader = new StreamReader(stream))
            {
                html = reader.ReadToEnd();
            }
            var jsonVar = JsonConvert.DeserializeObject<RateResponse>(html);
            //act and assert
            Assert.Equal(33, jsonVar.rates.Count);
        }
    }
}
