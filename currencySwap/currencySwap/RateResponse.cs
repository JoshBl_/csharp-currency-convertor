﻿using System.Collections.Generic;

namespace currencySwap
{
    public class RateResponse
    {
        //new dictionary created to help with looking up a value
        //key is string
        //value is float
        public Dictionary<string, float> rates { get; set; }
        //Dictionary is a good data structure for lookups
    }
}
