﻿using Newtonsoft.Json;
using Newtonsoft.Json.Schema;
using System;
using System.IO;
using System.Net;

namespace currencySwap
{
    class Program
    {
        //static strings, these can be accessed without creating an object of a class, but shared among all objects of that class.
        //static strings are shared for all instances of a class
        private static string currencyChoice;
        private static string html;
        private static double input;
        private static string userName;
        //static cannot be instantiated - so no new operator to create a variable of it!

        static void Main(string[] args)
        {
            //console title
            Console.Title = "The First National Bank of Josh";

            bool nameValid = false;

            //checking for valid username
            while (nameValid == false)
            {
                Console.WriteLine("Hello user! What is your name?");
                //setting user name in a string
                userName = Console.ReadLine();
                double nameChecker = 0;
                if (string.IsNullOrEmpty(userName) || double.TryParse(userName, out nameChecker))
                {
                    Console.WriteLine("That doesn't look right! Try again!");
                }
                else
                {
                    //printing the user name
                    Console.WriteLine("Ah - so your name is " + userName + "? Pleased to meet you!");
                    nameValid = true;
                }
            }
            Console.Write("Lets swap some currency!\n");

            //while loop to ensure that if the user enters anything except a string, it will loop until the user enters a string.
            bool attempt = false;
            while (attempt == false)
            {
                Console.WriteLine("Please set the base currency! Enter a three digit code (such as GBP, USD, JPY):");
                string userChoice = Console.ReadLine();
                currencyChoice = userChoice.ToUpper();
                double value = 0;
                if (double.TryParse(currencyChoice, out value) || string.IsNullOrEmpty(userChoice))
                {
                    Console.WriteLine("Error! Input is invalid! Try again!");
                }
                else
                {
                    Console.WriteLine("You have selected " + currencyChoice);
                    //grabbing the API
                    html = string.Empty;
                    string url = "https://api.exchangeratesapi.io/latest?base=";
                    url += currencyChoice;

                    //create local variable of HttpWebRequest, which initialises WebRequest
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    request.AutomaticDecompression = DecompressionMethods.GZip;

                    try
                    {
                        //new local variable to store the HTTP response
                        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                        //new local variable to read the body of the response from the server
                        using (Stream stream = response.GetResponseStream())
                        //new local variable to read characters from a byte stream
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            html = reader.ReadToEnd();
                            //sets bool to true, ends while loop
                            attempt = true;
                        }
                    }
                    //catches any errors
                    catch (Exception e)
                    {
                        Console.WriteLine("Error!");
                        Console.WriteLine(e);
                    }
                }
            }

            //setting the boolean value for check to true, to be used in the while loop
            bool check = true;

            //deserialize JSON to specified .NET type
            var jsonVar = JsonConvert.DeserializeObject<RateResponse>(html);
            //parse the html string into something more readable
            //create local variable schema
            JSchema schema = JSchema.Parse(html);

            //while loop set, checks if bool value is true
            while (check == true)
            {
                //menu - reads input from user
                Console.WriteLine("Main Menu - type a key to access the function");
                Console.Write("(V)iew currency rates\n");
                Console.Write("(S)wap currency\n");
                Console.Write("(P)rint rates\n");
                Console.Write("(Q)uit\n");
                
                //gets input from user
                string choice = Console.ReadLine();

                //converts to uppsercase to prevent errors
                string selectMenu = choice.ToUpper();

                //inserts the string of choice into the switch statement
                switch(selectMenu)
                {
                    case "V":
                        ViewCurrencyRates(jsonVar);
                        break;

                    case "S":
                        SwapCurrency(jsonVar);
                        break;

                    case "P":
                        PrintRates(schema);
                        break;

                    //quit application, changes bool check to false
                    case "Q":
                        Console.WriteLine("Exiting the application\n");
                        check = false;
                        Console.Write("So long, " + userName + "!");
                        Console.ReadKey();
                        break;

                    //if user types any other value, display error message
                    default:
                        Console.WriteLine("ERROR! Please type a valid command!");
                        break;
                }
            }
        }
        public static void ViewCurrencyRates(RateResponse entry)
        {
            //displays currency rate to user
            Console.WriteLine("View Currency Rates\n");
            //iterate through each currency in the JSON file and print the name and value
            foreach (var newCurrency in entry.rates)
            {
                Console.WriteLine(newCurrency.Key + " - " + newCurrency.Value);
            }
            Console.WriteLine("Press enter to return to Main Menu");
            Console.ReadKey();
        }

        public static void SwapCurrency(RateResponse entry)
        {
            //allows user to swap Pounds to different currency
            Console.WriteLine("Swap Currency\n");
            //booleans for while loops
            bool valueCheck = false;
            bool isInt = false;

            double newCheck;
            while (isInt == false)
            {
                Console.Write("Enter how much currency you want to swap!\n");
                string value = Console.ReadLine();
                if (double.TryParse(value, out newCheck))
                {
                    input = Convert.ToDouble(value);
                    isInt = true;
                }
                else
                {
                    Console.WriteLine("Error! Input is not a number!");
                }

            }

            Console.Write("You entered " + input + currencyChoice + "\n");

            while (valueCheck == false)
            {
                Console.Write("Type in a 3 digit code (such as GBP, USD, JPY) for which currency you want to compare with\n");
                try
                {
                    string temporary = Console.ReadLine();
                    //converting to upper to prevent errors
                    string compare = temporary.ToUpper();
                    //converting to upper to prevent errors
                    compare = temporary.ToUpper();
                    //accessing the rates dictionary to find a currency the user specified (and calculate how much money the user will receive)
                    double result = entry.rates[compare] * input;
                    //ensuring that the result is displayed with 2 decimal places
                    result = Math.Round(result, 2);
                    Console.Write("Here's what you can get for your money!\n");
                    Console.Write("For " + input + currencyChoice + " - you can get " + result + compare);
                    //prevents the screen from being filled with the main menu, allows user to focus on the data in front of them
                    Console.Write("\nPress enter to go back to menu!");
                    valueCheck = true;
                    Console.ReadKey();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error!");
                    Console.WriteLine(e);
                }
            }
        }

        public static void PrintRates(JSchema entry)
        {
            Console.WriteLine("Print rates\n");
            Console.Write("Please specify the location of where you want the file to be written to\n");
            //boolean for while loop
            bool printLoop = false;

            while (printLoop == false)
            {
                //obtain path for location where file should be written to
                string path = Console.ReadLine();
                //adding the file name and file format at the end
                path += "\\rates.txt";

                //check to make sure the file doesn't exist
                if (File.Exists(path))
                {
                    //print path in message
                    Console.WriteLine("ERROR! A file already exists here! " + path);
                    Console.WriteLine("Please try again!");
                }
                //if file doesn't exist...
                else
                {
                    //try-catch block for writing file and error handling
                    try
                    {
                        //confirm to user that file has been writtne, using the WriteAllText method
                        string file = Convert.ToString(entry);
                        File.WriteAllText(path, file);
                        Console.WriteLine("File Written: " + path);
                        Console.WriteLine("Press enter to return to Main Menu");
                        printLoop = true;
                        Console.ReadKey();
                    }
                    catch (Exception error)
                    {
                        //print error
                        Console.WriteLine(error);
                        Console.WriteLine("Plese try again!");
                    }
                }
            }
        }
    }
}